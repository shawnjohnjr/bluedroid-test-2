/* -*- Mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; tab-width: 40 -*- */
/* vim: set ts=2 et sw=2 tw=80: */
/* Copyright 2012 Mozilla Foundation and Mozilla contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef mozilla_dom_bluetooth_bluetoothgonkservice_h__
#define mozilla_dom_bluetooth_bluetoothgonkservice_h__

#include "BluetoothCommon.h"
#include "BluetoothDroidService.h"

BEGIN_BLUETOOTH_NAMESPACE

/**
 * BluetoothService functions are used to dispatch messages to Bluetooth DOM
 * objects on the main thread, as well as provide platform independent access
 * to BT functionality. Tasks for polling for outside messages will usually
 * happen on the IO Thread (see ipc/dbus for instance), and these messages will
 * be encased in runnables that will then be distributed via observers managed
 * here.
 */

class BluetoothDroidGonkService :  public BluetoothDroidService
{
public:
  bool IsReady();
  /**
   * Set up variables and start the platform specific connection. Must
   * be called from non-main thread.
   *
   * @return NS_OK if connection starts successfully, NS_ERROR_FAILURE otherwise
   */
  virtual nsresult StartInternal();

  /**
   * Stop the platform specific connection. Must be called from non-main thread.
   *
   * @return NS_OK if connection starts successfully, NS_ERROR_FAILURE otherwise
   */
  virtual nsresult StopInternal();

  /**
   * Get status of Bluetooth. Must be called from non-main thread.
   *
   * @return true if Bluetooth is enabled, false otherwise
   */
  virtual bool IsEnabledInternal();
  virtual void
    Connect(const nsAString& aDeviceAddress,
        uint32_t aCod,
        uint16_t aServiceUuid,
        BluetoothReplyRunnable* aRunnable) MOZ_OVERRIDE;
  virtual void
    Disconnect(const nsAString& aDeviceAddress, uint16_t aServiceUuid,
        BluetoothReplyRunnable* aRunnable) MOZ_OVERRIDE;

  virtual nsresult StartDiscoveryInternal(BluetoothReplyRunnable* aRunnable);
  virtual nsresult StopDiscoveryInternal(BluetoothReplyRunnable* aRunnable);
  virtual nsresult CreatePairedDeviceInternal(const nsAString& aDeviceAddress,
      int aTimeout, BluetoothReplyRunnable* aRunnable);
  virtual nsresult RemoveDeviceInternal(const nsAString& aDeviceAddress,
        BluetoothReplyRunnable* aRunnable);
  virtual bool SetPairingConfirmationInternal(const nsAString& aDeviceAddress, bool aConfirm, BluetoothReplyRunnable* aRunnable);
  virtual bool SetPinCodeInternal(const nsAString& aDeviceAddress, const nsAString& aPinCode,
                                                                 BluetoothReplyRunnable* aRunnable);

  virtual nsresult GetPairedDevicePropertiesInternal(const nsTArray<nsString>& aDeviceAddresses,
                                                                 BluetoothReplyRunnable* aRunnable);
  virtual nsresult GetDefaultAdapterPathInternal(BluetoothReplyRunnable* aRunnable);
  virtual nsresult SetProperty(BluetoothObjectType aType, const BluetoothNamedValue& aValue,
                               BluetoothReplyRunnable* aRunnable);
  virtual void
    SendMetaData(const nsAString& aTitle,
        const nsAString& aArtist,
        const nsAString& aAlbum,
        int64_t aMediaNumber,
        int64_t aTotalMediaCount,
        int64_t aDuration,
        BluetoothReplyRunnable* aRunnable) MOZ_OVERRIDE;

  virtual void
    SendPlayStatus(int64_t aDuration,
        int64_t aPosition,
        const nsAString& aPlayStatus,
        BluetoothReplyRunnable* aRunnable) MOZ_OVERRIDE;

  virtual void
    UpdatePlayStatus(uint32_t aDuration,
        uint32_t aPosition,
        ControlPlayStatus aPlayStatus) MOZ_OVERRIDE;

  virtual nsresult
    SendSinkMessage(const nsAString& aDeviceAddresses,
        const nsAString& aMessage) MOZ_OVERRIDE;

  virtual nsresult
    SendInputMessage(const nsAString& aDeviceAddresses,
        const nsAString& aMessage) MOZ_OVERRIDE;

private:
  /**
   *    * For DBus Control method of "UpdateNotification", event id should be
   *       * specified as following:
   *          * (Please see specification of AVRCP 1.33, Table 5.28 for more
   *          details.)
   *             */
  enum ControlEventId {
    EVENT_PLAYBACK_STATUS_CHANGED            = 0x01,
    EVENT_TRACK_CHANGED                      = 0x02,
    EVENT_TRACK_REACHED_END                  = 0x03,
    EVENT_TRACK_REACHED_START                = 0x04,
    EVENT_PLAYBACK_POS_CHANGED               = 0x05,
    EVENT_BATT_STATUS_CHANGED                = 0x06,
    EVENT_SYSTEM_STATUS_CHANGED              = 0x07,
    EVENT_PLAYER_APPLICATION_SETTING_CHANGED = 0x08,
    EVENT_UNKNOWN
  };

  virtual void UpdateNotification(ControlEventId aEventId, uint64_t aData);
};

END_BLUETOOTH_NAMESPACE

#endif
