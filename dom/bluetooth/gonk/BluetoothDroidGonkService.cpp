/* -*- Mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; tab-width: 40 -*- */
/* vim: set ts=2 et sw=2 tw=80: */
/* Copyright 2012 Mozilla Foundation and Mozilla contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifdef MOZ_B2G_BT_BLUEDROID
#include "base/basictypes.h"
#include "BluetoothDroidGonkService.h"
#include "nsDataHashtable.h"
#include "nsDebug.h"
#include "nsError.h"
#include "nsThreadUtils.h"
#include <dlfcn.h>
#include "BluetoothUtils.h"
//jb
#include <hardware/hardware.h>
#include <hardware/bluetooth.h>
#include "mozilla/dom/bluetooth/BluetoothTypes.h"
#include "BluetoothHfpManager.h"
#include "BluetoothOppManager.h"
#include "BluetoothUuid.h"
#include "BluetoothReplyRunnable.h"
//#include "hardware/bt_sock.h"
//
USING_BLUETOOTH_NAMESPACE

static bluetooth_device_t* bt_device;
const bt_interface_t* sBtInterface;
static bool IsBtEnabled = false;
static bool IsBtInterfaceInitialized = false;
//just for tests
static nsString sLocalName;
static nsString sLocalBdAddr;
static nsString sLastDomRequest;
static nsDataHashtable<nsStringHashKey, bool>* sPairedStateTable;
static nsDataHashtable<nsStringHashKey, BluetoothReplyRunnable*>* sDomReplyTable;
typedef char bdstr_t[18];

class PrepareProfileManagersRunnable : public nsRunnable
{
public:
  NS_IMETHOD
  Run()
  {
#if 0
    BluetoothHfpManager* hfp = BluetoothHfpManager::Get();
    BT_LOGD("++++++++++++++++HfpManager is Listening++++++++++++++++++");
    if (!hfp || !hfp->Listen()) {
      NS_WARNING("Failed to start listening for BluetoothHfpManager!");
      return NS_ERROR_FAILURE;
    }
#endif
    BluetoothOppManager* opp = BluetoothOppManager::Get();
    BT_LOGD("++++++++++++++++OppManager is Listening++++++++++++++++++");
    if (!opp || !opp->Listen()) {
      NS_WARNING("Failed to start listening for BluetoothOppManager!");
      return NS_ERROR_FAILURE;
    }
    return NS_OK;
  }
};

class PrepareAdapterRunnable : public nsRunnable
{
public:
  PrepareAdapterRunnable()
  {
    MOZ_ASSERT(NS_IsMainThread());
  }

  NS_IMETHOD
  Run()
  {
    MOZ_ASSERT(!NS_IsMainThread());
    NS_DispatchToMainThread(new PrepareProfileManagersRunnable());
    return NS_OK;
  }
};


class DefaultAdapterPropertiesRunnable : public nsRunnable
{
public:
  DefaultAdapterPropertiesRunnable(BluetoothReplyRunnable* aRunnable)
    : mRunnable(dont_AddRef(aRunnable))
  {
  }

  nsresult
  Run()
  {
    MOZ_ASSERT(!NS_IsMainThread());

    BluetoothValue v;
    nsAutoString replyError;
    v = InfallibleTArray<BluetoothNamedValue>();
    // We have to manually attach the path to the rest of the elements
    BT_LOGD("[Shawn]Preparing Bluetooth Adpapter");
    BT_LOGD("++++++[Shawn]Preparing Bluetooth Adpapter: %s", NS_ConvertUTF16toUTF8(sLocalName).get());

    v.get_ArrayOfBluetoothNamedValue().AppendElement(
        BluetoothNamedValue(NS_LITERAL_STRING("Name"), sLocalName));

    DispatchBluetoothReply(mRunnable, v, replyError);
    return NS_OK;
  }

private:
  nsRefPtr<BluetoothReplyRunnable> mRunnable;
};

class DistributeBluetoothSignalTask : public nsRunnable {
  BluetoothSignal mSignal;
public:
  DistributeBluetoothSignalTask(const BluetoothSignal& aSignal) :
    mSignal(aSignal)
  {
  }

  NS_IMETHOD
  Run()
  {
    MOZ_ASSERT(NS_IsMainThread());
    BluetoothService* bs = BluetoothService::Get();
    if (!bs) {
      NS_WARNING("BluetoothService not available!");
      return NS_ERROR_FAILURE;
    }
    BT_LOGD("-------------------------------------------------");
    BT_LOGD("---  DistributeBluetoothSignalTask  --");
    bs->DistributeSignal(mSignal);
    return NS_OK;
  }
};

static void moz_adapter_state_change_callback(bt_state_t status) {
  BT_LOGD("function %s ---- Callback: %d", __FUNCTION__, status);
  nsAutoString signalName;
  if (status == BT_STATE_ON) {
    BT_LOGD("BT turned on!!!!!!!!!!!!!!!!!!!!!!!!!!");
    IsBtEnabled = true;
    //signalName = NS_LITERAL_STRING("Enabled");
    signalName = NS_LITERAL_STRING("AdapterAdded");
    BT_LOGD("BT_STATE_OFF bluetooth signal distributed");
    nsRefPtr<PrepareProfileManagersRunnable> b = new PrepareProfileManagersRunnable();
    if (NS_FAILED(NS_DispatchToMainThread(b))) {
      NS_WARNING("Failed to dispatch to main thread!");
    }
  } else {
    IsBtEnabled = false;
    signalName = NS_LITERAL_STRING("Disabled");
    BT_LOGD("BT_STATE_OFF bluetooth signal distributed");
  }
  BluetoothValue aValue = true;
  BluetoothSignal signal(signalName, NS_LITERAL_STRING(KEY_MANAGER), aValue);
      nsRefPtr<DistributeBluetoothSignalTask>
            t = new DistributeBluetoothSignalTask(signal);
  if (NS_FAILED(NS_DispatchToMainThread(t))) {
    NS_WARNING("Failed to dispatch to main thread!");
  }

}

char *bd2str(bt_bdaddr_t *bdaddr, bdstr_t *bdstr)
{
  char *addr = (char *) bdaddr->address;

  sprintf((char*)bdstr, "%02x:%02x:%02x:%02x:%02x:%02x",
      (int)addr[0],(int)addr[1],(int)addr[2],
      (int)addr[3],(int)addr[4],(int)addr[5]);
  return (char *)bdstr;
}

void split_bd2str(bt_bdaddr_t *bdaddr, int num, int len)
{
  char* addr = (char *) bdaddr->address;
  char* addresses[num];
  int counts = 0;
  int j =0;
  for (int i=0; i<num; i+=6, j++) {
    BT_LOGD("i: %d", i);
    BT_LOGD("j: %d", j);
    sprintf((char*)addresses[j], "%02x:%02x:%02x:%02x:%02x:%02x",
        (int)addr[0+i],(int)addr[1+i],(int)addr[2+i],
        (int)addr[3+i],(int)addr[4+i],(int)addr[5+i]);
    BT_LOGD("bd address: %s", addresses[j]);
  }
}

int str2bd(const char *str, bt_bdaddr_t *addr)
{
  int32_t i = 0;
  for (i = 0; i < 6; i++) {
    addr->address[i] = (uint8_t) strtoul(str, (char **)&str, 16);
    str++;
  }
  return 0;
}

const char* class_to_icon(uint32_t classDevice) {
  switch ((classDevice & 0x1f00) >> 8) {
    case 0x01:
      return "computer";
    case 0x02:
      switch ((classDevice & 0xfc) >> 2) {
        case 0x01:
        case 0x02:
        case 0x03:
        case 0x05:
          return "phone";
        case 0x04:
          return "modem";
      }
      break;
    case 0x03:
      return "network-wireless";
    case 0x04:
      switch ((classDevice & 0xfc) >> 2) {
        case 0x01:
        case 0x02:
        case 0x06:
          return "audio-card";
        case 0x0b:
        case 0x0c:
        case 0x0d:
          return "camera-video";
        default:
          return "audio-card";
      }
      break;
    case 0x05:
      switch ((classDevice & 0xc0) >> 6) {
        case 0x00:
          switch ((classDevice && 0x1e) >> 2) {
            case 0x01:
            case 0x02:
              return "input-gaming";
          }
          break;
        case 0x01:
          return "input-keyboard";
        case 0x02:
          switch ((classDevice && 0x1e) >> 2) {
            case 0x05:
              return "input-tablet";
            default:
              return "input-mouse";
          }
      }
      break;
    case 0x06:
      if (classDevice & 0x80)
        return "printer";
      if (classDevice & 0x20)
        return "camera-photo";
      break;
  }
  return NULL;
}

static void moz_adapter_properties_callback(bt_status_t status,
    int num_properties, bt_property_t *properties) {
  bdstr_t bdstr;

  nsAutoString propertyName;
  BluetoothValue propertyValue;
  nsAutoString addrstr;
  InfallibleTArray<BluetoothNamedValue> aProperties;
  int type;
  BT_LOGD("+++++[shawn]FUNCTION: %s, num_prop: %d +++++", __FUNCTION__, num_properties);
  for (int i = 0; i < num_properties; i++) {
    BT_LOGD("Bluetooth properties type: %d", properties[i].type);

    if (properties[i].type == BT_PROPERTY_BDADDR) {
      BT_LOGD("Bluetooth Adapter properties type: BDADDR");
      BT_LOGD("[bluedroid] %s, Properties: %d, Address: %s", __FUNCTION__, num_properties, (const char*)bd2str((bt_bdaddr_t *)properties[i].val, &bdstr));
      sLocalBdAddr = NS_ConvertUTF8toUTF16(  (const char*) bd2str((bt_bdaddr_t *)properties[i].val, &bdstr) );
      propertyName.AssignASCII("Address");
      propertyValue = sLocalBdAddr;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));

    } else if (properties[i].type == BT_PROPERTY_BDNAME) {
      type = BT_PROPERTY_BDNAME;
      BT_LOGD("Bluetooth Adapter properties type: BDNAME");
      BT_LOGD("[bluedroid] %s, Properties: %d, Name: %s, length:%d", __FUNCTION__, num_properties, (const char*)properties[i].val, properties[i].len);
      uint32_t length = properties[i].len;
      char mystr[length];
      memcpy(mystr, (char*)properties[i].val, length);
      mystr[length] = '\0';
      propertyName.AssignASCII("Name");
      sLocalName = NS_ConvertUTF8toUTF16(mystr);
      propertyValue = sLocalName;
      BT_LOGD("Bluetooth Adapter property: %s", NS_ConvertUTF16toUTF8(sLocalName).get() );
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));
    } else if (properties[i].type == BT_PROPERTY_ADAPTER_SCAN_MODE) {
      BT_LOGD("Bluetooth Adapter properties type: SCAN_MODE");
      BT_LOGD("[bluedroid] %s, Properties: %d, Value: %d", __FUNCTION__, num_properties, *(uint32_t*)properties[i].val);
      propertyName.AssignASCII("Discoverable");
      propertyValue = *(uint32_t*)properties[i].val;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));
      return;
    } else if (properties[i].type == BT_PROPERTY_ADAPTER_BONDED_DEVICES) {
      //only returns paired addresses of devices, we need to request
      //remote_device properties again
      BT_LOGD("_____________________________________________________");
      BT_LOGD("Bluetooth Adapter properties type: BONDED DEVICES");
      BT_LOGD("BONDED DEVICES num of properties: %d", num_properties);
      BT_LOGD("BONDED DEVICES properties leng: %d", properties[i].len);
      if (properties[i].len == 0 || properties[i].len == 512) {
        // We don't know why length is 512, but it shall be 0 anyway
        // it looks like bug of bluedroid? Nothing we can do about it
        BT_LOGD("BONDED DEVICES is 0");
        BluetoothReplyRunnable* runnable;
        BluetoothValue values = InfallibleTArray<BluetoothNamedValue>();
        if (sDomReplyTable->Get(NS_LITERAL_STRING("GetPairedDevices"), &runnable)) {
          sDomReplyTable->Remove(NS_LITERAL_STRING("GetPairedDevices"));
          BT_LOGD("----->>>>  GetPairedDevices working!!!!!!!!<<<-----");
          DispatchBluetoothReply(runnable, values, EmptyString());
        } else {
          BT_LOGD("----->>>>  not reply GetPairedDevices property<<<<-----");
        }

        return;
      }
      int numberOfBDA = properties[i].len/6;
      BT_LOGD("[bluedroid] BONDED %s, Properties: %d, Address: %s", __FUNCTION__, num_properties, (const char*)bd2str((bt_bdaddr_t *)properties[i].val, &bdstr));
      sPairedStateTable->Put(NS_ConvertUTF8toUTF16((const char*)bd2str((bt_bdaddr_t *)properties[i].val, &bdstr)), true);
      split_bd2str((bt_bdaddr_t *)properties[i].val, numberOfBDA, properties[i].len);
      BT_LOGD("_____________________________________________________");
      if (sBtInterface) {
        //
        int ret = sBtInterface->get_remote_device_properties((bt_bdaddr_t *)properties[i].val);
        BT_LOGD("!!!!!! Get remote device properties!!!!!!");
        if (ret != BT_STATUS_SUCCESS) {
          BT_LOGD("Error while setting the callbacks, %s", __FUNCTION__);
          //sBtInterface = nullptr;
        }
        return;
      }
    } else {
      BT_LOGD("WTF?!");
    }
  }
  if (num_properties == 6) {
    //bluedroid will have 
    return;
  }

  //TODO: We got sent it back!
  BT_LOGD("!!![bluedroid - Shawn]bluetooth signal distributed Property Change");
  BT_LOGD("!!![bluedroid - Shawn ]bluetooth signal distributed Property Change");
  BluetoothValue aValue = aProperties;
  BluetoothSignal signal(NS_LITERAL_STRING("PropertyChanged"),NS_LITERAL_STRING(KEY_ADAPTER), aValue);
  nsRefPtr<DistributeBluetoothSignalTask>
    t = new DistributeBluetoothSignalTask(signal);
  if (NS_FAILED(NS_DispatchToMainThread(t))) {
    NS_WARNING("Failed to dispatch to main thread!");
  }

  if (type == BT_PROPERTY_BDNAME) {
    //let's reply dom request
    BluetoothReplyRunnable *runnable;
    if (sLastDomRequest.EqualsLiteral("SetName")) {
      sDomReplyTable->Get(NS_LITERAL_STRING("SetName"), &runnable);
      //Shawn
      // reply set property
      BT_LOGD("set adapter property");
      BluetoothValue v = aProperties;
      DispatchBluetoothReply(runnable, v, EmptyString());
    }
  }

}

static void moz_remote_device_properties_callback(bt_status_t status, bt_bdaddr_t *bd_addr, int num_properties, bt_property_t *properties) {
  /*
   *If bluetooth remote device changed, callback will be invoked, not only paired devices
   *
   * */

  BT_LOGD("moz remote device properties callback");
  nsAutoString propertyName;
  BluetoothValue propertyValue;
  bdstr_t bdstr;
  nsAutoString addrstr;
  nsTArray<nsString> deviceaddress;
  BluetoothValue values = InfallibleTArray<BluetoothNamedValue>();
  InfallibleTArray<BluetoothNamedValue> aProperties;
  BT_LOGD("[bluedroid] %s, Address: %s", __FUNCTION__, (const char*)bd2str(bd_addr, &bdstr));
  propertyName.AssignASCII("Address");
  BT_LOGD("[bluedroid] remote device num properties: %d", num_properties);
  addrstr = NS_ConvertUTF8toUTF16((const char*)bd2str(bd_addr, &bdstr));
  deviceaddress.AppendElement(addrstr);
  propertyValue = addrstr;
  aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));

  for (int i = 0; i < num_properties; i++) {
    BT_LOGD("Bluetooth properties type: %d", properties[i].type);
    if (properties[i].type == BT_PROPERTY_BDNAME) {
      BT_LOGD("[bluedroid] remote device properties %s, Properties: %d, Name: %s", __FUNCTION__, num_properties, (const char*)properties[i].val);
      propertyName.AssignASCII("Name");
      nsAutoString name = NS_ConvertUTF8toUTF16((const char*)properties[i].val);
      propertyValue = name;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));
    }
    if (properties[i].type == BT_PROPERTY_CLASS_OF_DEVICE) {
      BT_LOGD("+++++++++++>>>>>>Bluetooth remote device properties type: Class %d", *(uint32_t*)(properties[i].val));
      propertyName.AssignASCII("Class");
      propertyValue = *(uint32_t*)properties[i].val;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));

      const char* myicon = class_to_icon(*(uint32_t*)properties[i].val);
      BT_LOGD("----->>>>>>>>> remote device icon: %s", myicon);
      propertyName.AssignASCII("Icon");
      propertyValue = NS_ConvertUTF8toUTF16(myicon);
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));

    }
    if (properties[i].type == BT_PROPERTY_TYPE_OF_DEVICE) {
      BT_LOGD("Bluetooth remote device properties type of device %d", *(uint32_t*)properties[i].val);
      propertyName.AssignASCII("Type");
      propertyValue = *(uint32_t*)properties[i].val;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));

    }
    if (properties[i].type == BT_PROPERTY_SERVICE_RECORD) {
      BT_LOGD("Bluetooth service record channel: %s", (*(bt_service_record_t*)properties[i].val).name);
      //BT_LOGD("Bluetooth service record name: %s", ((bt_service_record_t*)properties[i].val).name);
    }

  }
  BluetoothValue val = aProperties;
  //we shall have reply *all* the paired device address instead of only one
  values.get_ArrayOfBluetoothNamedValue().AppendElement(
      BluetoothNamedValue(deviceaddress[0], val.get_ArrayOfBluetoothNamedValue()) );
  //let's reply dom request
  BluetoothReplyRunnable *runnable;
  if (num_properties>2 && sLastDomRequest.EqualsLiteral("GetPairedDevices")) {
    BluetoothValue v = values;
    if (sDomReplyTable->Get(NS_LITERAL_STRING("GetPairedDevices"), &runnable)) {
        sDomReplyTable->Remove(NS_LITERAL_STRING("GetPairedDevices"));
        BT_LOGD("----->>>>  GetPairedDevices working!!!!!!!!<<<-----");
        DispatchBluetoothReply(runnable, v, EmptyString());
    } else {
        BT_LOGD("----->>>>  not reply GetPairedDevices property<<<<-----");
    }
  }
}


void GetDeviceFoundProperty(int num_properties, bt_property_t *properties, InfallibleTArray<BluetoothNamedValue>& aProperties) {
  nsAutoString propertyName;
  BluetoothValue propertyValue;
  bdstr_t bdstr;
  nsAutoString addrstr;
  for (int i = 0; i < num_properties; i++) {
    BT_LOGD("Bluetooth properties type: %d", properties[i].type);
    if (properties[i].type == BT_PROPERTY_BDADDR) {
      BT_LOGD("[bluedroid] %s, Properties: %d, Address: %s", __FUNCTION__, num_properties, (const char*)bd2str((bt_bdaddr_t *)properties[i].val, &bdstr));
      propertyName.AssignASCII("Address");
      addrstr = NS_ConvertUTF8toUTF16((const char*)bd2str((bt_bdaddr_t *)properties[i].val, &bdstr));
      propertyValue = addrstr;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));
    }
    if (properties[i].type == BT_PROPERTY_BDNAME) {
      BT_LOGD("[bluedroid] %s, Properties: %d, Name: %s", __FUNCTION__, num_properties, (const char*)properties[i].val);
      propertyName.AssignASCII("Name");
      nsAutoString name = NS_ConvertUTF8toUTF16((const char*)properties[i].val);
      propertyValue = name;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));
    }
    if (properties[i].type == BT_PROPERTY_CLASS_OF_DEVICE) {
      BT_LOGD("Bluetooth Adapter properties type: Class %d", *(uint32_t*)properties[i].val);
      propertyName.AssignASCII("Class");
      propertyValue = *(uint32_t*)properties[i].val;
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));

      const char* myicon = class_to_icon(*(uint32_t*)properties[i].val);
      BT_LOGD("----->>>>>>>>> My icon: %s", myicon);
      propertyName.AssignASCII("Icon");
      propertyValue = NS_ConvertUTF8toUTF16(myicon);
      aProperties.AppendElement(BluetoothNamedValue(propertyName, propertyValue));

    }
    if (properties[i].type == BT_PROPERTY_TYPE_OF_DEVICE) {
      BT_LOGD("Bluetooth Adapter properties type: Class");
    }
    if (properties[i].type == BT_PROPERTY_SERVICE_RECORD) {
      //BT_LOGD("Bluetooth service record channel: %d", ((bt_service_record_t*)properties[i].val).channel);
      //BT_LOGD("Bluetooth service record name: %s", ((bt_service_record_t*)properties[i].val).name);
    }

  }
  BT_LOGD("!!!bluetooth signal distributed");
  BluetoothValue aValue = aProperties;
  BluetoothSignal signal(NS_LITERAL_STRING("DeviceFound"),NS_LITERAL_STRING(KEY_ADAPTER), aValue);
      nsRefPtr<DistributeBluetoothSignalTask>
            t = new DistributeBluetoothSignalTask(signal);
  if (NS_FAILED(NS_DispatchToMainThread(t))) {
    NS_WARNING("Failed to dispatch to main thread!");
  }

}

static void moz_device_found_callback(int num_properties, bt_property_t *properties) {
  InfallibleTArray<BluetoothNamedValue> aprop;
  GetDeviceFoundProperty(num_properties, properties, aprop);
}

static void moz_discovery_state_changed_callback(bt_discovery_state_t state) {
  BT_LOGD("%s", __FUNCTION__);
}

static void moz_pin_request_callback(bt_bdaddr_t *bd_addr, bt_bdname_t *bdname, uint32_t cod) {
  BT_LOGD("++++++++%s++++++++", __FUNCTION__);
  nsAutoString propertyName;
  BluetoothValue propertyValue;
  InfallibleTArray<BluetoothNamedValue> aprop;
  bdstr_t bdstr;
  nsAutoString addrstr = NS_ConvertUTF8toUTF16((const char*)bd2str(bd_addr, &bdstr));

  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("address"), addrstr));
  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("method"), NS_LITERAL_STRING("pincode")));
  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("name"), NS_ConvertUTF8toUTF16((const char*)bdname->name)));
  BluetoothValue aValue = aprop;
  BluetoothSignal signal(NS_LITERAL_STRING("RequestPinCode"),NS_LITERAL_STRING(KEY_LOCAL_AGENT), aValue);
      nsRefPtr<DistributeBluetoothSignalTask>
            t = new DistributeBluetoothSignalTask(signal);
  if (NS_FAILED(NS_DispatchToMainThread(t))) {
    NS_WARNING("Failed to dispatch to main thread!");
  }

}

static void moz_ssp_request_callback(bt_bdaddr_t *bd_addr, bt_bdname_t *bdname, uint32_t cod, bt_ssp_variant_t pairing_variant, uint32_t pass_key) {
  BT_LOGD("++++++++ %s ++++++++++", __FUNCTION__);
  nsAutoString propertyName;
  BluetoothValue propertyValue;
  InfallibleTArray<BluetoothNamedValue> aprop;
  bdstr_t bdstr;
  nsAutoString addrstr = NS_ConvertUTF8toUTF16((const char*)bd2str(bd_addr, &bdstr));

  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("address"), addrstr));
  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("method"), NS_LITERAL_STRING("confirmation")));
  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("passkey"), pass_key));
  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("name"), NS_ConvertUTF8toUTF16((const char*)bdname->name)));
  BluetoothValue aValue = aprop;
  BluetoothSignal signal(NS_LITERAL_STRING("RequestConfirmation"),NS_LITERAL_STRING(KEY_LOCAL_AGENT), aValue);
      nsRefPtr<DistributeBluetoothSignalTask>
            t = new DistributeBluetoothSignalTask(signal);
  if (NS_FAILED(NS_DispatchToMainThread(t))) {
    NS_WARNING("Failed to dispatch to main thread!");
  }

}

static void moz_bond_state_changed_callback(bt_status_t status, bt_bdaddr_t *bd_addr, bt_bond_state_t state) {
  BT_LOGD("%s", __FUNCTION__);
  BT_LOGD(" Bonding state: %d", (bt_bond_state_t)state);

  InfallibleTArray<BluetoothNamedValue> aprop;
  bdstr_t bdstr;
  nsAutoString addrstr = NS_ConvertUTF8toUTF16((const char*)bd2str(bd_addr, &bdstr));
  //settings checked bluetooth-pairedstatuschanged
  aprop.AppendElement(BluetoothNamedValue(NS_LITERAL_STRING("paired"), true));
  BluetoothValue aValue = aprop;
  BluetoothSignal signal(NS_LITERAL_STRING("PairedStatusChanged"),NS_LITERAL_STRING(KEY_LOCAL_AGENT), aValue);
      nsRefPtr<DistributeBluetoothSignalTask>
            t = new DistributeBluetoothSignalTask(signal);
  if (NS_FAILED(NS_DispatchToMainThread(t))) {
    NS_WARNING("Failed to dispatch to main thread!");
  }

}

static void moz_acl_state_changed_callback(bt_status_t status, bt_bdaddr_t *bd_addr, bt_acl_state_t state) {
  BT_LOGD("%s", __FUNCTION__);
}

static void moz_callback_thread_event(bt_cb_thread_evt event) {
  BT_LOGD("%s", __FUNCTION__);
}

bt_callbacks_t sBluetoothCallbacks = {
  sizeof(sBluetoothCallbacks),
  moz_adapter_state_change_callback,
  moz_adapter_properties_callback,
  moz_remote_device_properties_callback,
  moz_device_found_callback,
  moz_discovery_state_changed_callback,
  moz_pin_request_callback,
  moz_ssp_request_callback,
  moz_bond_state_changed_callback,
  moz_acl_state_changed_callback,
  moz_callback_thread_event,
};

static bool
EnsureBluetoothHalLoad()
{
  int err = 0;
  hw_module_t* module;
  hw_device_t* device;
  BT_LOGD("!!!!!!!!!Loading HAL lib!!!!!!!!!!!!!!!!");
  err = hw_get_module(BT_HARDWARE_MODULE_ID, (hw_module_t const**)&module);
  if (err == 0) {
    module->methods->open(module, BT_HARDWARE_MODULE_ID, &device);
    if (err == 0) {
      bt_device= (bluetooth_device_t *)device;
      sBtInterface = bt_device->get_bluetooth_interface();
    } else {
      BT_LOGD("Error while opening bluetooth library");
    }
  } else {
    BT_LOGD("No Bluetooth library found");
  }
  BT_LOGD("!!!!!!!HAL loaded!!!!");
  return true;
}

static bool
EnsureBluetoothHalUnload()
{
  int err = 0;
  sBtInterface = nullptr;
  BT_LOGD("HAL library unloaded (%s)", strerror(err));
  return err;
}

static nsresult
StartStopGonkBluetooth(bool aShouldEnable)
{
  BT_LOGD("++++++++++StartStopGonkBluetooth+++++++++++++++");
  bool result;

  // Platform specific check for gonk until object is divided in
  // different implementations per platform. Linux doesn't require
  // bluetooth firmware loading, but code should work otherwise.
  if (!EnsureBluetoothHalLoad()) {
    NS_ERROR("Failed to load bluedroid library.\n");
    return NS_ERROR_FAILURE;
  }

  if (sBtInterface && !IsBtInterfaceInitialized) {
    int ret = sBtInterface->init(&sBluetoothCallbacks);
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks %s", __FUNCTION__);
      sBtInterface = nullptr;
      return NS_ERROR_FAILURE;
    } else {
      IsBtInterfaceInitialized = true;
    }
  }

  //WTF just test
  if (!aShouldEnable) {
    BT_LOGD("Disabling BT!!!!!!!!!!!!!");
    int ret = sBtInterface->disable();
    return (ret == BT_STATUS_SUCCESS) ? NS_OK : NS_ERROR_FAILURE;
  }
  //enable bluetooth
  int ret = sBtInterface->enable();
  if (ret == BT_STATUS_SUCCESS) {
    BT_LOGD("Enabling is working!!!!!!!");
  } else {

  }
  return (ret == BT_STATUS_SUCCESS) ? NS_OK : NS_ERROR_FAILURE;

}

bool
BluetoothDroidGonkService::IsReady()
{
  MOZ_ASSERT(!NS_IsMainThread());
  if (!IsBtEnabled) {
    BT_WARNING("Bluetooth service is not ready yet!");
    return false;
  }
  return true;
}

nsresult
BluetoothDroidGonkService::StartInternal()
{
  BT_LOGD("++++ %s ++++++", __FUNCTION__);
  MOZ_ASSERT(!NS_IsMainThread());

  nsresult ret;
  if (!sPairedStateTable) {
    sPairedStateTable = new nsDataHashtable<nsStringHashKey, bool>;
  }

  if (!sDomReplyTable) {
    sDomReplyTable = new nsDataHashtable<nsStringHashKey, BluetoothReplyRunnable*>;
  }

  ret = StartStopGonkBluetooth(true);

  if (NS_FAILED(ret)) {
    BT_LOGD("++++ %s ++++++ return ret", __FUNCTION__);
    return ret;
  }
  return NS_OK;
}

nsresult
BluetoothDroidGonkService::StopInternal()
{
  BT_LOGD("++++ %s ++++++", __FUNCTION__);
  MOZ_ASSERT(!NS_IsMainThread());
  if (sDomReplyTable) {
    sDomReplyTable->Clear();
  }
  nsresult ret;
  ret = StartStopGonkBluetooth(false);
  if (NS_FAILED(ret)) {
    return ret;
  }
  return NS_OK;
}

bool
BluetoothDroidGonkService::IsEnabledInternal()
{
  MOZ_ASSERT(!NS_IsMainThread());

  if (!EnsureBluetoothHalLoad()) {
    NS_ERROR("Failed to load bluedroid library.\n");
    return false;
  }
  return IsBtEnabled;
}

/*
 *StartDiscoveryInternal
 *
 */
nsresult
BluetoothDroidGonkService::StartDiscoveryInternal(BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("++++++++++++++++++++++++++++++Start Discovery ++++++++++++++++++++++++++++++");
  MOZ_ASSERT(NS_IsMainThread());
  if (sBtInterface) {
    int ret = sBtInterface->start_discovery();
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks, %s", __FUNCTION__);
      sBtInterface = nullptr;
      return NS_ERROR_FAILURE;
    }
  }
  BluetoothValue v = true;
  nsAutoString errorStr;
  DispatchBluetoothReply(aRunnable, v, errorStr);
  return NS_OK;
}

/*
 *StopDiscoveryInternal
 * TODO: Why can't we get request back
 */
nsresult
BluetoothDroidGonkService::StopDiscoveryInternal(BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("++++++++++++++++++++++++++++++Stop Discovery ++++++++++++++++++++++++++++++");
  MOZ_ASSERT(NS_IsMainThread());
  if (sBtInterface) {
    int ret = sBtInterface->cancel_discovery();
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks, %s", __FUNCTION__);
      sBtInterface = nullptr;
      return NS_ERROR_FAILURE;
    }
  }
  BluetoothValue v = true;
  nsAutoString errorStr;
  DispatchBluetoothReply(aRunnable, v, errorStr);
  return NS_OK;
}

nsresult
BluetoothDroidGonkService::CreatePairedDeviceInternal(
    const nsAString& aDeviceAddress,
    int aTimeout,
    BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("+++++++++++++++++++++   %s  +++++++++++++++++++++++++++", __FUNCTION__);
  MOZ_ASSERT(NS_IsMainThread());
  if (sBtInterface) {
    bt_bdaddr_t remote_addr;
    nsCString tempDeviceAddress = NS_ConvertUTF16toUTF8(aDeviceAddress);
    const char *deviceAddress = tempDeviceAddress.get();
    str2bd(deviceAddress, &remote_addr);

    int ret = sBtInterface->create_bond(&remote_addr);
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks, %s", __FUNCTION__);
      sBtInterface = nullptr;
      return NS_ERROR_FAILURE;
    }
  }

  return NS_OK;
}

/*
 *SetPairingConfirmation--->
 *
 */
bool
BluetoothDroidGonkService::SetPairingConfirmationInternal(
    const nsAString& aDeviceAddress,
    bool aConfirm,
    BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("+++++++++++++++++++++   %s  +++++++++++++++++++++++++++", __FUNCTION__);
  MOZ_ASSERT(NS_IsMainThread());
  if (sBtInterface) {
    bt_bdaddr_t remote_addr;
    nsCString tempDeviceAddress = NS_ConvertUTF16toUTF8(aDeviceAddress);
    const char *deviceAddress = tempDeviceAddress.get();
    str2bd(deviceAddress, &remote_addr);

    int ret = sBtInterface->ssp_reply(&remote_addr, (bt_ssp_variant_t)0, aConfirm, 0);
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks");
      sBtInterface = nullptr;
      //return NS_ERROR_FAILURE;
      return false;
    }
  }

  return true;
}

nsresult
BluetoothDroidGonkService::RemoveDeviceInternal(const nsAString& aDeviceAddress,
                                               BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("+++++++++++++++++++++   %s  +++++++++++++++++++++++++++", __FUNCTION__);
  MOZ_ASSERT(NS_IsMainThread());
  if (sBtInterface) {
    bt_bdaddr_t remote_addr;
    nsCString tempDeviceAddress = NS_ConvertUTF16toUTF8(aDeviceAddress);
    const char *deviceAddress = tempDeviceAddress.get();
    str2bd(deviceAddress, &remote_addr);

    int ret = sBtInterface->remove_bond(&remote_addr);
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks");
      sBtInterface = nullptr;
      return NS_ERROR_FAILURE;
    }
  }

  return NS_OK;

}

nsresult
BluetoothDroidGonkService::GetPairedDevicePropertiesInternal(const nsTArray<nsString>& aDeviceAddresses,
                                                            BluetoothReplyRunnable* aRunnable)
{
  // Get paired devices were all required to do two steps:
  // 1. Get bonded devices addresses
  // 2. Use bonded devices addresses to query remote devices
  // So we do BluetoothReply only until we actually get bonded devices properties
  BT_LOGD("+++++++++++++++++++++   %s  +++++++++++++++++++++++++++", __FUNCTION__);
  MOZ_ASSERT(NS_IsMainThread());
  if (!IsReady()) {
    NS_NAMED_LITERAL_STRING(errorStr, "Bluetooth service is not ready yet!");
    DispatchBluetoothReply(aRunnable, BluetoothValue(), errorStr);
    return NS_OK;
  }

  if (sBtInterface) {
    int ret = sBtInterface->get_adapter_property(BT_PROPERTY_ADAPTER_BONDED_DEVICES);
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks, %s", __FUNCTION__);
      sBtInterface = nullptr;
      return NS_ERROR_FAILURE;
    }
  }
  //TODO: we shall check if we already had GetPairedDevices method call
  if (sDomReplyTable)
    sDomReplyTable->Put(NS_LITERAL_STRING("GetPairedDevices"), aRunnable);
  sLastDomRequest = NS_LITERAL_STRING("GetPairedDevices");

  return NS_OK;
}

nsresult
BluetoothDroidGonkService::GetDefaultAdapterPathInternal(BluetoothReplyRunnable* aRunnable)
{

  nsRefPtr<BluetoothReplyRunnable> runnable = aRunnable;
#if 0
  nsRefPtr<nsRunnable> func(new DefaultAdapterPropertiesRunnable(runnable));
  if (NS_FAILED(mBluetoothCommandThread->Dispatch(func, NS_DISPATCH_NORMAL))) {
    NS_WARNING("Cannot dispatch firmware loading task!");
    return NS_ERROR_FAILURE;
  }
#endif
  BluetoothValue v;
  nsAutoString replyError;
  v = InfallibleTArray<BluetoothNamedValue>();
  // We have to manually attach the path to the rest of the elements
  BT_LOGD("[Shawn]Preparing Bluetooth Adpapter");
  BT_LOGD("++++++[Shawn]Preparing Bluetooth Adpapter: %s", NS_ConvertUTF16toUTF8(sLocalName).get());

  v.get_ArrayOfBluetoothNamedValue().AppendElement(
      BluetoothNamedValue(NS_LITERAL_STRING("Name"), sLocalName));

  DispatchBluetoothReply(aRunnable, v, replyError);


  runnable.forget();
  return NS_OK;

}

nsresult
BluetoothDroidGonkService::SetProperty(BluetoothObjectType aType,
                                      const BluetoothNamedValue& aValue,
                                      BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("!!!!!!!!!!!!!SetProperty!!!!!!!!!!");
  NS_ASSERTION(NS_IsMainThread(), "Must be called from main thread!");

  nsCString intermediatePropName(NS_ConvertUTF16toUTF8(aValue.name()));
  const char* propName = intermediatePropName.get();
  BT_LOGD("[bluedroid]Property Name: %s", propName);


  nsCString str;
  int len;
  const char* tempStr;
  char mystr[255];
  if (aValue.value().type() == BluetoothValue::Tuint32_t) {
    BT_LOGD("Value int: %d", aValue.value().get_uint32_t() );
    bt_property_t prop;
    prop.type = BT_PROPERTY_ADAPTER_DISCOVERY_TIMEOUT;
    prop.val = (void*)aValue.value().get_uint32_t();
    sBtInterface->set_adapter_property(&prop);
  } else if (aValue.value().type() == BluetoothValue::TnsString) {
    str = NS_ConvertUTF16toUTF8(aValue.value().get_nsString());
    BT_LOGD("---------------- temp str len: %d  -------------------------", str.Length());
    tempStr = str.get();
    len = str.Length();
    memcpy(mystr, tempStr, len);
    mystr[len]='\0';
    BT_LOGD("Value string: %s", mystr);
    bt_property_t prop;
    prop.type = BT_PROPERTY_BDNAME;
    prop.val = mystr;
    prop.len = str.Length();
    BT_LOGD("________ ______prop.val: %s, prop.lenlen: %d", prop.val, prop.len);
    if (sDomReplyTable)
      sDomReplyTable->Put(NS_LITERAL_STRING("SetName"), aRunnable);
    sLastDomRequest = NS_LITERAL_STRING("SetName");
    sBtInterface->set_adapter_property(&prop);

  } else if (aValue.value().type() == BluetoothValue::Tbool) {
    bt_property_t prop;
    prop.type = BT_PROPERTY_ADAPTER_SCAN_MODE;
    if (aValue.value().get_bool() == true) {
      BT_LOGD("Value bool: 1");
      bt_scan_mode_t mode = BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE;
      bt_scan_mode_t* sss = &mode;
      prop.val = (void *)sss;
      prop.len = 4;
      sBtInterface->set_adapter_property(&prop);
    } else {
      BT_LOGD("Value bool: 0");
      bt_scan_mode_t mode = BT_SCAN_MODE_CONNECTABLE;
      bt_scan_mode_t* sss = &mode;
      prop.val = (void*)sss;
      prop.len = 4;
      sBtInterface->set_adapter_property(&prop);
    }
  } else {
   NS_WARNING("Property type not handled!");
  }
  return NS_OK;
}

bool
BluetoothDroidGonkService::SetPinCodeInternal(const nsAString& aDeviceAddress,
                                         const nsAString& aPinCode,
                                         BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("+++++++++++++++++++++   %s  +++++++++++++++++++++++++++", __FUNCTION__);
  MOZ_ASSERT(NS_IsMainThread());
  if (sBtInterface) {
    bt_bdaddr_t remote_addr;
    nsCString tempDeviceAddress = NS_ConvertUTF16toUTF8(aDeviceAddress);
    const char *deviceAddress = tempDeviceAddress.get();
    str2bd(deviceAddress, &remote_addr);

    int ret = sBtInterface->pin_reply(&remote_addr, true, aPinCode.Length(),  (bt_pin_code_t*) NS_ConvertUTF16toUTF8(aPinCode).get()  );
    if (ret != BT_STATUS_SUCCESS) {
      BT_LOGD("Error while setting the callbacks");
      sBtInterface = nullptr;
      return false;
    }
  }
  return true;
}

void
BluetoothDroidGonkService::Connect(const nsAString& aDeviceAddress,
                                 uint32_t aCod,
                                 uint16_t aServiceUuid,
                                 BluetoothReplyRunnable* aRunnable)
{
  BT_LOGD("+++++++++++++++++++++BluetoothDroidGonkService   %s  +++++++++++++++++++++++++++", __FUNCTION__);
#if 0
  MOZ_ASSERT(NS_IsMainThread());
  MOZ_ASSERT(aRunnable);

  BluetoothServiceClass serviceClass =
    BluetoothUuidHelper::GetBluetoothServiceClass(aServiceUuid);

  if (sController) {
    DispatchBluetoothReply(aRunnable, BluetoothValue(),
        NS_LITERAL_STRING(ERR_NO_AVAILABLE_RESOURCE));
    return;
  }

  sController =
    new BluetoothProfileController(aDeviceAddress, aRunnable,
        DestroyBluetoothProfileController);
  if (aServiceUuid) {
    sController->Connect(serviceClass);
  } else {
    sController->Connect(aCod);
  }
#endif
}

void
BluetoothDroidGonkService::Disconnect(const nsAString& aDeviceAddress,
                                 uint16_t aServiceUuid,
                                 BluetoothReplyRunnable* aRunnable)
{
  MOZ_ASSERT(NS_IsMainThread());
  MOZ_ASSERT(aRunnable);
#if 0
  BluetoothServiceClass serviceClass =
    BluetoothUuidHelper::GetBluetoothServiceClass(aServiceUuid);

  if (sController) {
    DispatchBluetoothReply(aRunnable, BluetoothValue(),
        NS_LITERAL_STRING(ERR_NO_AVAILABLE_RESOURCE));
    return;
  }

  sController =
    new BluetoothProfileController(aDeviceAddress, aRunnable,
        DestroyBluetoothProfileController);
  if (aServiceUuid) {
    sController->Disconnect(serviceClass);
  } else {
    sController->Disconnect();
  }
#endif
}

void
BluetoothDroidGonkService::SendMetaData(const nsAString& aTitle,
    const nsAString& aArtist,
    const nsAString& aAlbum,
    int64_t aMediaNumber,
    int64_t aTotalMediaCount,
    int64_t aDuration,
    BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidGonkService::SendPlayStatus(int64_t aDuration,
    int64_t aPosition,
    const nsAString& aPlayStatus,
    BluetoothReplyRunnable* aRunnable)
{
}

void
BluetoothDroidGonkService::UpdatePlayStatus(uint32_t aDuration,
    uint32_t aPosition,
    ControlPlayStatus aPlayStatus)
{
}

void
BluetoothDroidGonkService::UpdateNotification(ControlEventId aEventId,
                                                 uint64_t aData)
{
}

nsresult
BluetoothDroidGonkService::SendSinkMessage(const nsAString& aDeviceAddresses,
                      const nsAString& aMessage)
{
  return NS_OK;
}

nsresult
BluetoothDroidGonkService::SendInputMessage(const nsAString& aDeviceAddresses,
                      const nsAString& aMessage)
{
  return NS_OK;
}

#endif
